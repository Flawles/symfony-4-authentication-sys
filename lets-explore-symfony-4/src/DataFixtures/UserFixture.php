<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixture extends Fixture
{
    public $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setFirstname('Hamza');
        $user->setLastname('Touhs');
        $user->setUsername('Dogueto');
        $user->setPassword(
            $this->encoder->encodePassword($user, '123456')
        );
        $user->setMail('dogueto@lol.fr');

        $manager->persist($user);

        $manager->flush();
    }
}
